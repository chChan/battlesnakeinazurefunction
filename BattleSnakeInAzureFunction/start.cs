using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Starter.Api.Requests;

namespace BattleSnakeInAzureFunction
{
    public static class start
    {
        /// <summary>
        /// Your Battlesnake will receive this request when it has been entered into a new game.
        /// Every game has a unique ID that can be used to allocate resources or data you may need.
        /// Your response to this request will be ignored.
        /// </summary>
        [FunctionName("start")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = "bs/start")] HttpRequest req,
            ILogger log)
        { 
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            GameStatusRequest gameStatusRequest = JsonConvert.DeserializeObject<GameStatusRequest>(requestBody);

            return new OkObjectResult(new object());
        }


  

    }
}
