using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Starter.Api.Requests;
using Starter.Api.Responses;
using System.Collections.Generic;
using System.Linq;
using Starter.Api;

namespace BattleSnakeInAzureFunction
{
    public static class move
    {

        /// <summary>
        /// This request will be sent for every turn of the game.
        /// Use the information provided to determine how your
        /// Battlesnake will move on that turn, either up, down, left, or right.
        /// </summary>
        [FunctionName("move")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = "bs/move")] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            string name = req.Query["name"];

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            GameStatusRequest gameStatusRequest = JsonConvert.DeserializeObject<GameStatusRequest>(requestBody);


            //version 1, eat food only, without any forcast
            var direction = new List<string> { "down", "left", "right", "up" };

            var snakeMove = "down";

            var moveResponse = new MoveResponse
            {
                Move = snakeMove,
                Shout = "I am moving!"
            };

            return new OkObjectResult(moveResponse);
        }
    }
}
