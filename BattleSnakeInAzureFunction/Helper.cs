﻿using System;

namespace Starter.Api
{
    public static class Helper
    {
        public static int GetDistance(Starter.Api.Coordinate obj1Coordinate, Starter.Api.Coordinate obj2Coordinate)
        {
            var distanceX = obj1Coordinate.X - obj2Coordinate.X;
            var distanceY = obj1Coordinate.Y - obj2Coordinate.Y;
            return Math.Abs(distanceX) + Math.Abs(distanceY);
        }
    }
}
