using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Starter.Api.Responses;

namespace BattleSnakeInAzureFunction
{
    public static class Initialize
    {

        /// <summary>
        /// This request will be made periodically to retrieve information about your Battlesnake,
        /// including its display options, author, etc.
        /// </summary>
        [FunctionName("Initialize")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = "bs")] HttpRequest req,
            ILogger log)
        {
            var initReponse = new InitResponse
            {
                ApiVersion = "1",
                Author = "",
                Color = "#255355",
                Head = "default",
                Tail = "default"
            };

            return new OkObjectResult(initReponse);
        }
    }
}
